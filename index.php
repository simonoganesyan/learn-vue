<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    <style type="text/css">
        table {
            width: 100%;
        }
        td, th {
            border: 1px solid greenyellow;
            text-align: left;
            padding: 5px;
        }
    </style>
</head>
<body>
<div id="app">
    <p>Общее кол-во юзеров: {{ count }}</p>
    <p><button @click="toggleTable">{{ buttonText }}</button></p>
    <table v-if="showTable">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>sname</th>
                <th>fname</th>
                <th>avatar</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="user in users">
                <td>{{ user.id }}</td>
                <td>{{ user.name | capitalize }}</td>
                <td>{{ user.sname | capitalize }}</td>
                <td>{{ user.fname | capitalize }}</td>
                <td>
                    <img v-if="user.avatar" :src="user.avatar" width="30px" />
                    <span v-else>нет аватары</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    var vm = new Vue({
        el: '#app',
        data: {
            users: [
                {id: 1, name: 'alex', sname: 'Petrovich', fname: 'ivanov', avatar: 'http://avatars.mitosa.net/Ellete/ellete_0549.jpg'},
                {id: 2, name: 'petr', sname: 'Ivanovich', fname: 'Sidorov', avatar: ''},
                {id: 3, name: 'Luis', sname: 'Nazario', fname: 'Lima', avatar: 'http://avatars.mitosa.net/Ellete/ellete_0549.jpg'},
                {id: 4, name: 'rivaldo', sname: 'De', fname: 'Barbosa', avatar: 'http://avatars.mitosa.net/Ellete/ellete_0549.jpg'},
                {id: 5, name: 'Sun', sname: 'sunych', fname: 'Kipelov', avatar: 'http://avatars.mitosa.net/Ellete/ellete_0549.jpg'},
            ],
            showTable: true
        },
        methods: {
            toggleTable() {
                this.showTable = !this.showTable
            }
        },
        computed: {
            count(){
                return this.users.length
            },
            buttonText(){
                return this.showTable ? 'Скрыть' : 'Показать'
            }
        },
        filters: {
            capitalize: function (value) {
                if (!value) return ''
                value = value.toString()
                return value.charAt(0).toUpperCase() + value.slice(1)
            }
        }
    })
</script>
</body>
</html>